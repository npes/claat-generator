author:            Nikolaj Simonsen
summary:           Guide on basic HTML, CSS and JavaScript
id:                html_css_js_guide
categories:        static page
environments:      markdown, go
status:            alpha
feedback link:     nisi@ucl.dk
analytics account: 0

# HTML, CSS, JavaScript basics

## HTML

Duration: 30

Text of the slide/step
(follows markdown syntax)

## CSS

Duration: 20

## JavaScript

Duration: 45

Text of the slide/step
(follows markdown syntax)

You can use links also (again following the markdown syntax)
[10000 game](https://npes.gitlab.io/10000_game/).

Pictures can be also embedded.
![tooltip for picture](images/html_css_js_guide/nisi.png)

Videos can be added but only as links to source using same syntax as url.