author:            Nikolaj Simonsen
summary:           Dashboards using HTML, CSS and JavaScript
id:                dashboard_workshop
categories:        static page
environments:      html, css, js
status:            beta
feedback link:     https://gitlab.com/npes/dashboard_thingspeak/issues
analytics account: 0

# Dashboards with HTML, CSS and JavaScript

## Introduction

Duration: 10

Welcome to the dashboard workshop. This workshop will help you create custom dashboards from HTML, CSS and javascript.

HTML, CSS and javascript are the building blocks of frontend web applications and an understanding of it is essential for building for the web.

The examples used in the workshop uses [thingspeak](https://thingspeak.com/) as api and data storage. [thingspeak](https://thingspeak.com/) is very easy to use but it has a limit in that you can only push data to it every 15 seconds. This is fine for data collection from simple sensors, but if you need realtime performance you should build you own API and use that instead.

If you want to try this dashboard with your own [thingspeak](https://thingspeak.com/) channel it is free to create one.

Data comes from a [bosch bme280](https://www.bosch-sensortec.com/bst/products/all_products/bme280) sensor that is connected via i2c to a raspberry pi. [bosch bme280](https://www.bosch-sensortec.com/bst/products/all_products/bme280) can measure temperature, humidity and pressure(hPa). 

The raspberry pi is running a [node.js](https://nodejs.org/en/) application that is sending data from the sensor to a [thingspeak](https://thingspeak.com/) channel using [thingspeaks](https://thingspeak.com/) api.

Finally a webpage is fetching the data from the [thingspeak](https://thingspeak.com/) channel, again using [thingspeaks](https://thingspeak.com/) api.

A simple block diagram of the parts in the solution can be seen here:

![workshop blockdiagram](images/dashboard_workshop/dashboard_workshop_blockdiagram.png)

The workshop is divided in to 3 parts:
* part 1 tells you how to setup the development environment
* part 2 gives you example code for a simple dashboard and lets you explore HTML, CSS and javascript
* part 3 tells you how to use node.js on a raspberry pi and a javascript example to push data to an api

## Requirements

- Skills
    - Basic HTML and CSS skills
    - Basic programming skills (from any language)
    - Basic command line skills (for setting up a raspberry pi)
    
- Hardware    
    - Raspberry pi with internet access
    - A sensor to collect some data
    - Computer

## Setup visual studio code

Duration: 15

Before you can work with the code you need an editor. For this workshop you will use visual studio code. You can grap a copy of visual studio code [here](https://code.visualstudio.com/)

To get the basics of visual studio code please watch this short video

![https://www.youtube.com/watch?v=SYRwSyjD8oI](images/dashboard_workshop/youtube.png)

If you need more videos on visual studio code you can try to take a look at the official [intro video series](https://www.youtube.com/watch?v=SYRwSyjD8oI&list=PLj6YeMhvp2S5UgiQnBfvD7XgOMKs3O_G6) or better yet, consult the [documentation](https://code.visualstudio.com/docs)

`TIP!` Visual studio code is great for other languages besides HTML, CSS and JavaScript

## Install live server extension

Duration: 15

When you edit code and want to preview it in the browser, it quickly becomes annoying having to refresh the browser manually. Luckily there is an extension for visual studio code, called `Live Server` that runs your webpage on a local server, and refreshes the browser every time you save your changes in a document.

To install the live server extension:
1. Press `Ctrl+Shift+X` or use the icon to open the extensions pane in visual studio code
2. Use the search field to search for `Live Server` (author: Ritwick Dey) 
3. Click the install button
4. Reload visual studio code if prompted to do so

![Live Server Install](images/dashboard_workshop/live_server_install.png)

## Download and test dashboard example code

Duration: 30

The example code for the dashboard is on gitlab.

Clone or download the code from [https://gitlab.com/npes/dashboard_thingspeak](https://gitlab.com/npes/dashboard_thingspeak)

Open the folder containing the code from within visual studio code using the file menu

![open folder](images/dashboard_workshop/vsc_open_folder.png)

Once the example is open you should see the below folder structure

![folder structure](images/dashboard_workshop/folder.png)

The css folder contains the cascading style sheet `style.css` used to style the HTML

The javascript folder contains the javascript files

- `script.js` invokes functions and listens for events (button clicks etc.)
- `constants.js` holds all the constants 
- `functions.js` has all the functions
- `chart.js` & `utils.js` is used for the [chart js javascript library](https://www.chartjs.org/)

`index.html` has all the HTML

Test that it works in the browser by right clicking `index.html` and choose `open with live server`

![open with live server](images/dashboard_workshop/live_server_open.png)

If it works you should see something similar to the below screenshot in your browser

![dashboard example](images/dashboard_workshop/dashboard_example.png)

## HTML

Duration: 20

If you need to brush up on your HTML skills [w3schools](https://www.w3schools.com/html/) is a great resource.

Inspect `index.html` in visual studio code to get an overview of the different elements.

As an example the code below is for the "card" named "Button Card Title"

```
<div class="col-md-4 card-container pb-5">
    <div class="h-100 shadow card col-md-12 mx-auto">
        <div class="card-body">
            <h5 class="card-title">Button card title</h5>
            <h6 class="card-subtitle mb-2 text-muted">Button card subtitle</h6>
            
            <div class="card-buttons row mb-3">
                <button class="btn btn-secondary btn-lg btn-raised buttons" id="on-btn">ON</button>
                <button class="btn btn-secondary btn-lg btn-raised buttons" id="off-btn">OFF</button>
            </div>
            <div class="row">
                <div class="col-sm-12 status-field" id="status-field-1">n/a</div>
            </div>
        </div>
    </div>
</div>
``` 
If you compare it to the graphics you should be able to identify what the different HTML elements represents. Notice that most of the HTML elements has a ```class=""``` attached and some of them has and ```id=""``` attached.

Classes can be used on more than one HTML element and id can be used to uniquely identify an HTML element.

Both classes and id's can be targeted from a css file to change the elements apperance, and from javascript to add functionality. 

![card](images/dashboard_workshop/card.png)

Another way of inspecting HTML is by using you browsers development tools. In Chrome you either right click the page and click "inspect" or use the shortcut `Ctrl+Shit+I`

![inspector](images/dashboard_workshop/inspector.png)

## CSS

Duration: 15

The example dashboard is using a variant of the bootstrap framework called [Bootstrap Material Design](https://fezvrasta.github.io/bootstrap-material-design/) which uses classes to style the elements and make the page responsive.

Responsive web pages automatically adapts to the screen size so your dashboard can be used across devices. To test this you can try to resize the browser window.

![responsive dashboard](images/dashboard_workshop/responsive.png)

[Bootstrap Material Design](https://fezvrasta.github.io/bootstrap-material-design/) makes it really easy to style elements. You just add the appropiate class to the element and it changes appereance.

Take a look at the [Bootstrap Material Design Documentation](https://fezvrasta.github.io/bootstrap-material-design/docs/4.0/getting-started/introduction/) to learn how it works. 

Try to change a few elements to learn how to use it.

The file style.css is regular css. You can learn more about css at [w3schools CSS](https://www.w3schools.com/css/)

You can use the Chrome developer tools to inspect css. Right click an element and chose inspect.

![cssinspect](images/dashboard_workshop/cssinspect.png)

## JavaScript

Duration: 120

Ok, you reached where the real magic happens, the Javascript part. Congratulations!

If you have no previous knowledge about JavaScript please spend some time at [w3schools JavaScript](https://www.w3schools.com/js/default.asp)

Next thing to do, is to inspect `script.js` which is the "main" javascript file.

At `line 1 and 2` you see theese lines 

```
import {updateGraph, updateHttpLog, getChannelField, updateThingSpeakField, initGraph, updateStatusField} from './functions.js'
import {FeedUpdateUrl, fieldUrl, writeApiKey, field4, field5} from './constants.js'
``` 

This imports functions from other javascript files and is like include files in `C` or modules in `Python`. This helps seperate and encapsulate logic and keeps file sizes manageable.

At `line 5` a javascript plugin called JQuery is used to wait for the [Document Object Model (DOM)](https://www.w3schools.com/js/js_htmldom.asp) to become ready before executing the code in the curly braces.

```
$(document).ready(function(){
```

Read more about JQuery and what it is [here](https://jquery.com/)

`Line 7 - 14` uses JQuery to traverse the DOM and put html elements into JS variables.

Notice how the elements ID is targeted using the # symbol. If you wanted to target a class you would use the . symbol

```
let onButton = $('#on-btn');
let offButton = $('#off-btn');
let switch1 = $('#switch1');
let updateButton = $('#update-btn');
let sendButton = $('#send-btn');
let statusField1 = $('#status-field-1');
let statusField3 = $('#status-field-3');
let slider1 = $('#slider1');
```

`Line 20 - 25` listens for click events from the #on-btn element

```
onButton.click(async () => {
    let updateResponse = await updateThingSpeakField(FeedUpdateUrl, writeApiKey, field4, '1');
    updateHttpLog(updateResponse);
    let channelFieldResponse = await getChannelField(fieldUrl, field4);
    updateStatusField(channelFieldResponse.field4.toString(), statusField1);
});
```

When the button is clicked an anonymous asynchronious function is executed. This is what the ```async () =>``` syntax means. This is also called [arrow](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions) functions.

The reason for the function to be asynchron is that clicking the button triggers an HTTP get request which might take some time. 

Async functions returns something called a promise, which is like an answer that will happen at some point in the future when the request either resolves or rejects. If it wasn't async the code would be blocked and not able to proceed before the request returned.

It is a little abstract to understand async functions, but if you are curious (and you are, right!) you can read more about [async function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function) 

`Line 22` updates the HTTP log element with the HTTP response from the request.

`Line 23` fetches the latest data entry from field 4 in the [thingspeak](https://thingspeak.com/channels/731067) api and stores it in the variable channelFIeldResponse.

Finally `line 24` updates the element with an ID of #status-field-1

### Tasks

* Investigate how the fetch api works to make HTTP requests [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)
* Investigate the `httpGet(url)` function in `functions.js`
* Read about chart js and it's possibilities [Chart JS](https://www.chartjs.org/)
* Investigate the `updateGraph()` function in `functions.js`
* Investigate the `chart.js` javascript file to learn how the chart is configured.
* Try to change the number of datapoint in the graph to 15 elements instead of 10

bonus

* Make a bar chart using [Chart JS](https://www.chartjs.org/)
* Create you own [thingspeak](https://thingspeak.com/) channel

TIP! Use Chromes developer tools to see the javascript console. This will display errors and you can use ```console.log()``` in your javascript code, to output variables and other data to the console.

![inspectjs](images/dashboard_workshop/inspectjs.png)

Developer tools can also be used to set breakpoints and singlestep javascript code, use the `sources` tap in developer tools.

The screenshot shows a breakpoint inserted into line 23 in script.js

![singlestep](images/dashboard_workshop/singlestep.png)

Now you know how to make a dashboard, but you still need to know how to send data to an api with javascript

## Node.js on raspberry pi

Duration: 15

Node.js is using the javascript engine from Chrome, it is called [V8](https://v8.dev/)
This means you can execute javascript without using Chrome. 

To get started with node.js on Raspberry pi install it using this guide [https://www.w3schools.com/nodejs/nodejs_raspberrypi.asp](https://www.w3schools.com/nodejs/nodejs_raspberrypi.asp) 
Skip to the `Install Node.js on Raspberry Pi` chapter if your raspberry is already installed.

Continue the guide to learn more about node.js on raspberry pi. 

You can also try the [node.js tutorial](https://www.w3schools.com/nodejs/default.asp)

node.js comes with a package manager that is called [NPM](https://www.w3schools.com/whatis/whatis_npm.asp) 

NPM is the world's largest Software Registry with over 800,000 code packages!

## Node.js example code

Duration: 15

The example code is using a [bosch bme280](https://www.bosch-sensortec.com/bst/products/all_products/bme280) communicating with the raspberry through [I2C](https://en.wikipedia.org/wiki/I%C2%B2C)

If you have a [bme280](https://www.bosch-sensortec.com/bst/products/all_products/bme280) connect it to the raspberry's I2C pins and enable I2C on the raspberry. [This](https://www.raspberrypi-spy.co.uk/2016/07/using-bme280-i2c-temperature-pressure-sensor-in-python/) article has more information about the connections to a raspberry pi 2. 

The [bme280-sensor](https://www.npmjs.com/package/bme280-sensor) NPM package makes it really easy to communicate with the sensor.

Clone the example code from this [repository](https://gitlab.com/npes/bme280_nodejs_thingspeak) to your raspberry pi.

To execute it type `node app` in the applications directory.

If the script is running with a [bme280](https://www.bosch-sensortec.com/bst/products/all_products/bme280) sensor the output looks like this

![nodejs](images/dashboard_workshop/nodejs.png)

This code example is, like the dashboard, using the [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch) to make HTTP requests so you should be able to find your way through it.

`Disclaimer!` The channel and api key will disappear soon, you should use this example with your own api or thingspeak channel

## Your own node.js application

### Tasks

Duration: 60

* Create either a Thingspeak account and channel, or even better use your own API
* Modify the node.js example to fit your own sensor and API/Thingspeak channel
* Use [NPM](https://www.npmjs.com/) to search for a package for your sensor.

TIP! If you want to modify the node.js code with visual studio code from your computer, mount the raspberry pi folder on your computer. If you are using windows you can [set up Samba file sharing on a Raspberry Pi](https://www.juanmtech.com/samba-file-sharing-raspberry-pi/)

## Conclusion

Duration: 0

This codelab is a very brief introduction to the languages of the web, HTML, CSS and JavaScript. 

Javascript is the only true cross platform language and the potential is amazing. If you are missing strong types you can try [TypesScript](https://www.typescriptlang.org/), a language that implements the newest javascipt functionality and transpiles to pure, compatible javascript.

Typescript is developed by Danish [Anders Hejlsberg](https://en.wikipedia.org/wiki/Anders_Hejlsberg) who also invented Turbo Pascal and had a big part in the Delphi and C# languages.

I hope you enjoyed the code lab and continue to use web technologies for your IoT projects!