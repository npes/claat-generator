author:            Nikolaj Simonsen
summary:           Markdown edit and preview
id:                markdown_edit_preview
categories:        static page
environments:      markdown, visual studio code
status:            beta
feedback link:     https://gitlab.com/npes/claat-generator/issues
analytics account: 0

# Markdown edit and preview guide

## Introduction

### Purpose

The purpose of this document is to give lecturers and students an overview of how to preview and edit markdown (.md) files. 

### Description

The workflow described is using visual studio code, an open source editor, that can edit a large variety of text files.  
Visual studio code has built-in side by side preview of the formatted .md document which gives a better overview when editing .md files.  
This document covers only the very basic use of visual studio code and the markdown syntax. 

## Pre-requisites

### Setup visual studio code

Visual studio code needs to be installed on your computer.   
You can grap a copy of visual studio code [here](https://code.visualstudio.com/)

To learn how to install and basic use of visual studio code, please visit the [visual studio code basics documentation](https://code.visualstudio.com/docs/introvideos/basics)

If you need more information on visual studio code, consult the [documentation](https://code.visualstudio.com/docs)

**INFO** Visual studio code is great for other things, besides editing .md files

## Editing

1. Create a new .md document or clone the [test repository](https://gitlab.com/npes/test_repository) which contains a sample markdown file, `markdown_cheat_sheet.md`
2. Open it with visual studio code  
**INFO** If you have more than one .md file in the same folder you can open the entire folder in visual studio code
3. Use the keyboard shortcut `ctrl+k` and then `v` to open the .md preview side by side with the .md file you are editing.

This is what it looks like when editing and previewing .md files in visual studio code side by side.  
The .md file is on the left and the preview is on the right.

![side by side edit preview](images/markdown_edit_preview_guide/markdown_preview.png "side by side edit preview")